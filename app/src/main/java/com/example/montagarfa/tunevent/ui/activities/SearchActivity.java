package com.example.montagarfa.tunevent.ui.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.dataServices.EventWebService;
import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.netowrk.RetrofitClientInstance;
import com.example.montagarfa.tunevent.ui.recyclerView.HomeRecyclerViewAdapter;
import com.example.montagarfa.tunevent.ui.recyclerView.SearchRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SearchActivity";
    ArrayList<Event> events= new ArrayList<Event>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Log.d(TAG, "onCreate: started.");
        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.color.colorTI);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progressDialog.dismiss();
                    }
                }, 1000);
        initListEvent();
    }

    private void initListEvent(){
        Log.d(TAG,"ini list event: preparing .") ;

        String location=null;
        String date=null;
        String category =null;
        getIntentValues(location,date,category);
        EventWebService eventData = RetrofitClientInstance.getRetrofitInstance().create(EventWebService.class);
        Call<List<Event>> eventList = eventData.getAllEvents();
        eventList.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                Log.d(TAG, "onResponse: "+response.code());
                if (response.isSuccessful()){
                    for (Event event : response.body()) {
                        events.add(event);
                        initRecyledView();
                    }
                }else{
                    Toast.makeText(getBaseContext(), "Failed to load data", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Toast.makeText(getBaseContext(), "Failed to load data", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void initRecyledView(){
        System.out.println("garfa montassar");
        Log.d(TAG,""+events.size());
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recylce_view_searcj);
        // RecycleViewAdapter adapter = new RecycleViewAdapter(this, mNames, mImageUrls);
        //  SearchRecyclerViewAdapter adapter = new SearchRecyclerViewAdapter(this,listEvent);
        SearchRecyclerViewAdapter adapter1 = new SearchRecyclerViewAdapter(this,events);
        recyclerView.setAdapter(adapter1);
        System.out.println("garfa monta");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }
    public void getIntentValues(String locV,String dateV,String catV){
        if(getIntent().hasExtra("dateSearch"))
            locV=getIntent().getStringExtra("dateSearch");
        else{
            locV = "ALL";
        }
        if(getIntent().hasExtra("locationSearch"))
            dateV=getIntent().getStringExtra("locationSearch");
        else {
            dateV="ALL";
        }
        if(getIntent().hasExtra("categorySearch"))
            catV=getIntent().getStringExtra("categorySearch");
        else {
            catV="ALL";
        }
        Log.d(TAG, "initListEvent: "+locV);
        Log.d(TAG, "initListEvent: "+dateV);
        Log.d(TAG, "initListEvent: "+catV);
        System.out.println("init app");
    }

}