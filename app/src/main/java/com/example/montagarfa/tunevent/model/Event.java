package com.example.montagarfa.tunevent.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable {
    @SerializedName("idEvent")
    private String idEvent;
    @SerializedName("nameEvent")
    private String nameEvent;
    @SerializedName("Location")
    private String location;
    @SerializedName("Date")
    private Date date;
    @SerializedName("nbOfPlace")
    private Long nbOfPlace;
    @SerializedName("nbOfDplace")
    private Long nbOfDplace;
    @SerializedName("organizer")
    private User organizer;
    @SerializedName("category")
    private Category category;
    @SerializedName("photo_uri")
    private String photo_uri;

    public Event() {
    }

    public Event(String nameEvent, String location, java.util.Date date, Long nbOfPlace, Long nbOfDplace, User organizer, Category category,String photo_uri) {
        this.nameEvent = nameEvent;
        this.location = location;
        this.date = date;
        this.nbOfPlace = nbOfPlace;
        this.nbOfDplace = nbOfDplace;
        this.organizer = organizer;
        this.category = category;
        this.photo_uri=photo_uri;
    }

    public Event(String idEvent, String nameEvent, String location, java.util.Date date, Long nbOfPlace, Long nbOfDplace, User organizer, Category category,String photo_uri) {
        this.idEvent = idEvent;
        this.nameEvent = nameEvent;
        this.location = location;
        this.date = date;
        this.nbOfPlace = nbOfPlace;
        this.nbOfDplace = nbOfDplace;
        this.organizer = organizer;
        this.category = category;
        this.photo_uri=photo_uri;
    }

    protected Event(Parcel in) {
        idEvent = in.readString();
        nameEvent = in.readString();
        location = in.readString();
        if (in.readByte() == 0) {
            nbOfPlace = null;
        } else {
            nbOfPlace = in.readLong();
        }
        if (in.readByte() == 0) {
            nbOfDplace = null;
        } else {
            nbOfDplace = in.readLong();
        }
        organizer = in.readParcelable(User.class.getClassLoader());
        category = in.readParcelable(Category.class.getClassLoader());
        photo_uri = in.readString();
    }



    public String getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(String idEvent) {
        this.idEvent = idEvent;
    }

    public String getNameEvent() {
        return nameEvent;
    }

    public void setNameEvent(String nameEvent) {
        this.nameEvent = nameEvent;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getNbOfPlace() {
        return nbOfPlace;
    }

    public void setNbOfPlace(Long nbOfPlace) {
        this.nbOfPlace = nbOfPlace;
    }

    public Long getNbOfDplace() {
        return nbOfDplace;
    }

    public void setNbOfDplace(Long nbOfDplace) {
        this.nbOfDplace = nbOfDplace;
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getPhoto_uri() {
        return photo_uri;
    }

    public void setPhoto_uri(String photo_uri) {
        this.photo_uri = photo_uri;
    }

   /* @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
         /*String idEvent;
         String nameEvent;
         String location;
         Date date;
         Long nbOfPlace;
         Long nbOfDplace;
         User organizer;
         Category category;
        parcel.writeString(idEvent);
        parcel.writeString(nameEvent);
        parcel.writeString(location);

    }*/
}
