package com.example.montagarfa.tunevent.ui.recyclerView;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.ui.activities.EventActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SearchRecyclerViewAdapter extends RecyclerView.Adapter<SearchRecyclerViewAdapter.ViewHolder> {
    private static String TAG = "SearchRecyclerViewAdapter";

    private Context mContext;
    private ArrayList<Event> lEvent = new ArrayList<Event>();

    public SearchRecyclerViewAdapter(Context mContext, ArrayList<Event> lEvent) {
        this.mContext = mContext;
        this.lEvent = lEvent;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view,parent,false);
        ViewHolder viewHolder =new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG,"On bind view holder : called");
        Glide.with(mContext)
                .asBitmap()
                .load(lEvent.get(position).getPhoto_uri())
                .into(holder.eventImage);
        holder.eventTitle.setText(lEvent.get(position).getNameEvent());
        holder.eventCategory.setText("category");
        holder.eventLocation.setText(lEvent.get(position).getLocation());
        DateFormat formatData = new SimpleDateFormat("dd-MMM-yy");
        String dateString = formatData.format(new Date());

        holder.eventDate.setText(dateString);
        //holder.eventDate.setText(lEvent.get(position).getDate().toString());
        holder.eventContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"On Click: clicked on: "+ lEvent.get(position).getNameEvent());

                Toast.makeText(mContext, lEvent.get(position).getNameEvent(),Toast.LENGTH_SHORT).show();
                Intent intent= new Intent(mContext,EventActivity.class);
                intent.putExtra("event",lEvent.get(position));

                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return lEvent.size();
    }

    public class  ViewHolder extends RecyclerView.ViewHolder {
        ImageView eventImage;
        TextView eventTitle;
        TextView eventCategory;
        TextView eventLocation;
        TextView eventDate;
        LinearLayout eventContainer;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            eventContainer = itemView.findViewById(R.id.event_container);
            eventImage = itemView.findViewById(R.id.event_image);
            eventTitle= itemView.findViewById(R.id.event_title);
            eventCategory= itemView.findViewById(R.id.event_category);
            eventLocation= itemView.findViewById(R.id.event_location);
            eventDate= itemView.findViewById(R.id.event_date);
        }
    }
}
