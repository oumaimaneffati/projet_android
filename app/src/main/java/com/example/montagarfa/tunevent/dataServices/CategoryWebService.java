package com.example.montagarfa.tunevent.dataServices;

import com.example.montagarfa.tunevent.model.Category;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface CategoryWebService {
    @GET("api/category/all")
    Call<List<Category>> getAllCategory();

    @GET("api/category/{id}")
    Category getCategoryById(String id);

    @POST("api/category")
    Category addCategory(@Body Category event);

    @DELETE("api/category/{id}")
    void deleteCategoryById(String id);
}
