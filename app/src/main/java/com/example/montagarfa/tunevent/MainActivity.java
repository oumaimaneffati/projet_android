package com.example.montagarfa.tunevent;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.ui.fragments.AloginFragment;
import com.example.montagarfa.tunevent.ui.fragments.HomeFragment;
import com.example.montagarfa.tunevent.ui.fragments.ProfileFragment;
import com.example.montagarfa.tunevent.ui.fragments.SearchFragment;
import com.example.montagarfa.tunevent.ui.recyclerView.HomeRecyclerViewAdapter;
import com.example.montagarfa.tunevent.ui.recyclerView.SearchRecyclerViewAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    SharedPreferences sharedPreferences;
    private String sharedpreffile="com.example.montagarfa.tunevent";
    private static String TAG=" Main   Activity  ";
    // list of events
    private ArrayList<Event> listEvent=new ArrayList<Event>();
    //vars
   // private ArrayList<String> mNames= new ArrayList<String>();
   // private ArrayList<String> mImageUrls = new ArrayList<String>();
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences=getSharedPreferences(sharedpreffile,MODE_PRIVATE);
        Log.d(TAG,"onCreate :started");
        ButterKnife.bind(this);
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        loadFragment(new SearchFragment());
      //  initImagesBitmaps();
      //  initListEvents();



        /*EventWebService eventData = RetrofitClientInstance.getRetrofitInstance().create(EventWebService.class);
        Call<List<Event>> eventList = eventData.getAllEvents();
        System.out.println("init app");
        eventList.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                System.out.println("all is Ok");
                System.out.println(response.code());
                System.out.println(response.body());
                for (Event event : response.body()) {
                    System.out.println(event.getNameEvent());
                }

            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                System.out.println("error");
                System.out.println(t);
            }
        });
        Call<Event> event = eventData.getEventById("5c0bd0a372a2aa46c5f97cb5111");
        event.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                System.out.println("it's ok");
               // System.out.println(response.body().getNameEvent());
                System.out.println(response.body());
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                System.out.println("error");
                System.out.println(t);
            }
        });
*/
    }
    private void initListEvents(){
        Log.d(TAG,"ini list event: preparing .") ;
        Event event = new Event("Concert","Tunisie",new Date(),Long.valueOf(20),Long.valueOf(20),null,null,"https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        for (int i =0;i<10;i++){
            listEvent.add(event);
        }
        initRecyledView();
    }

    /*private void initImagesBitmaps(){
        Log.d(TAG,"initImagesBitmaps: preparing bitmaps.");

        mImageUrls.add("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        mNames.add("Havasu Falls");

        mImageUrls.add("https://i.redd.it/tpsnoz5bzo501.jpg");
        mNames.add("Trondheim");

        mImageUrls.add("https://i.redd.it/qn7f9oqu7o501.jpg");
        mNames.add("Portugal");

        mImageUrls.add("https://i.redd.it/j6myfqglup501.jpg");
        mNames.add("Rocky Mountain National Park");


        mImageUrls.add("https://i.redd.it/0h2gm1ix6p501.jpg");
        mNames.add("Mahahual");

        mImageUrls.add("https://i.redd.it/k98uzl68eh501.jpg");
        mNames.add("Frozen Lake");


        mImageUrls.add("https://i.redd.it/glin0nwndo501.jpg");
        mNames.add("White Sands Desert");

        mImageUrls.add("https://i.redd.it/obx4zydshg601.jpg");
        mNames.add("Austrailia");

        mImageUrls.add("https://i.imgur.com/ZcLLrkY.jpg");
        mNames.add("Washington");

        initRecyledView();
    }*/
    private void initRecyledView(){
        System.out.println("garfa montassar");
        Log.d(TAG,""+listEvent.size());
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recylce_view);
       // RecycleViewAdapter adapter = new RecycleViewAdapter(this, mNames, mImageUrls);
      //  SearchRecyclerViewAdapter adapter = new SearchRecyclerViewAdapter(this,listEvent);
        HomeRecyclerViewAdapter adapter1 = new HomeRecyclerViewAdapter(this,listEvent);
        recyclerView.setAdapter(adapter1);
        System.out.println("garfa monta");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.navigtion_home:
                fragment = new HomeFragment();
                break;
            case R.id.navigtion_profile:
            { if(sharedPreferences.getString("logged_in","").isEmpty())
                fragment = new ProfileFragment();
            else fragment = new AloginFragment();
            }
                break;
            case R.id.navigtion_search:
                fragment = new SearchFragment();
                break;
        }
        return loadFragment(fragment);
    }
}
