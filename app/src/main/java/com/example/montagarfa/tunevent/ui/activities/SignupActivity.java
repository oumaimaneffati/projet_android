package com.example.montagarfa.tunevent.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.montagarfa.tunevent.MainActivity;
import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.ui.fragments.AloginFragment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    private String sharedpreffile="com.example.montagarfa.tunevent";
    private static final String TAG = "SignupActivity";
    @BindView(R.id.input_firstname)
    EditText firstname;
    @BindView(R.id.input_lastname)
    EditText lastname;
    @BindView(R.id.input_email)
    EditText email;
    @BindView(R.id.input_password)
    EditText password;
    @BindView(R.id.input_reEnterPassword)
    EditText repassword;
    @BindView(R.id.input_photo_uri)
    EditText photo_uri;
    @BindView(R.id.btn_signup)
    AppCompatButton signup;
    @BindView(R.id.link_login)
    TextView login;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        sharedPreferences=getSharedPreferences(sharedpreffile,MODE_PRIVATE);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                signup();
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

            }
        });
    }
    //signup bottom clicked
    public void signup(){
        if (!validate()) {
            onSignupFailed();
            return;
        }


        String emailV= email.getText().toString();
        String passwordV= password.getText().toString();
        // TODO: 11/12/18
        sharedPreferences.edit().putString("logged_in","ok").commit();
        /*getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new AloginFragment())
                .commit();*/
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

    }
    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        signup.setEnabled(true);
    }

    //validate signup
    public boolean validate() {
        boolean valid = true;

        String firstanameV = firstname.getText().toString();
        String lastnameV = lastname.getText().toString();
        String emailV = email.getText().toString();
        String passwordV = password.getText().toString();
        String repasswordV = repassword.getText().toString();
        String photo_uriV = photo_uri.getText().toString();

        if (firstanameV.isEmpty() || lastnameV.length() < 3) {
            firstname.setError("at least 3 chafragment_containerracters");
            valid = false;
        } else {
            firstname.setError(null);
        }

        if (lastnameV.isEmpty() || lastnameV.length() < 3) {
            lastname.setError("at least 3 characters");
            valid = false;
        } else {
            lastname.setError(null);
        }



        if (emailV.isEmpty()) {
            email.setError("enter a valid email address");
            valid = false;
        } else {
            email.setError(null);
        }

        if (passwordV.isEmpty() || passwordV.length() < 4 || passwordV.length() > 10) {
            password.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            password.setError(null);
        }

        if (repasswordV.isEmpty() || repasswordV.length() < 4 || repasswordV.length() > 10 || !(repasswordV.equals(passwordV))) {
            repassword.setError("Password Do not match");
            valid = false;
        } else {
            repassword.setError(null);
        }
        if (photo_uriV.isEmpty() ) {
            photo_uri.setError("enter a valid uri");
            valid = false;
        } else {
            photo_uri.setError(null);
        }

        return valid;
    }
}

