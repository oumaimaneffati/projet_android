package com.example.montagarfa.tunevent.model;

import com.google.gson.annotations.SerializedName;

public class Booking {
    @SerializedName("idBooking")
    private String idBooking;
    @SerializedName("Event")
    private Event event;
    @SerializedName("usr")
    private User user;
    @SerializedName("nbOfBPlace")
    private Long nbOfBPlace;

    public Booking() {
    }

    public Booking(Event event, User user, Long nbOfBPlace) {
        this.event = event;
        this.user = user;
        this.nbOfBPlace = nbOfBPlace;
    }

    public Booking(String idBooking, Event event, User user, Long nbOfBPlace) {

        this.idBooking = idBooking;
        this.event = event;
        this.user = user;
        this.nbOfBPlace = nbOfBPlace;
    }

    public String getIdBooking() {
        return idBooking;
    }

    public void setIdBooking(String idBooking) {
        this.idBooking = idBooking;
    }

    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getNbOfBPlace() {
        return nbOfBPlace;
    }

    public void setNbOfBPlace(Long nbOfBPlace) {
        this.nbOfBPlace = nbOfBPlace;
    }
}
