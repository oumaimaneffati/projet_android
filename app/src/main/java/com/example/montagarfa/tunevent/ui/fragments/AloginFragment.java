package com.example.montagarfa.tunevent.ui.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.dataServices.CategoryWebService;
import com.example.montagarfa.tunevent.dataServices.UserWebService;
import com.example.montagarfa.tunevent.model.Category;
import com.example.montagarfa.tunevent.model.User;
import com.example.montagarfa.tunevent.netowrk.RetrofitClientInstance;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class AloginFragment extends Fragment {
    private static final String TAG = "AloginFragment";
    SharedPreferences sharedPreferences;
    private String sharedpreffile="com.example.montagarfa.tunevent";
    User appuser;
    @BindView(R.id.email_user)
    TextView email;
    @BindView(R.id.firstname_user)
    TextView firstname;
    @BindView(R.id.lastname_user)
    TextView lastname;
    @BindView(R.id.image_user)
    ImageView image;
    @BindView(R.id.btn_logout)
    Button btnLogOut;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alogin,null);
        ButterKnife.bind(this,view);
        sharedPreferences=getActivity().getSharedPreferences(sharedpreffile,MODE_PRIVATE);

        getUser();
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        return view;
    }
    public void logout(){

        final ProgressDialog progressDialog = new ProgressDialog(getActivity(),
                R.color.colorTI);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Log Out...");
        progressDialog.show();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        //monta.garfa@gmail.com onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 1000);

        // TODO: 11/12/18
        sharedPreferences.edit().clear().commit();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new ProfileFragment())
                .commit();
    }
    private void getUser(){
        UserWebService userData = RetrofitClientInstance.getRetrofitInstance().create(UserWebService.class);
        Call<User> user1 = userData.getUser();
        System.out.println("init app");
        user1.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "onResponse: "+response.isSuccessful());
                Log.d(TAG, "onResponse: "+response.code());
                if( response.isSuccessful()){
                    appuser = response.body();
                }
                email.setText(appuser.getUsername());
               firstname.setText(appuser.getFirstName());
               lastname.setText(appuser.getLastName());
/*               Glide.with(getContext())
                        .asBitmap()
                        .load("https://i.redd.it/j6myfqglup501.jpg")
                        .into(image);*/
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

    }
}
