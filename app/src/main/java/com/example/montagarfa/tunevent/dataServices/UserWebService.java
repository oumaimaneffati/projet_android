package com.example.montagarfa.tunevent.dataServices;

import com.example.montagarfa.tunevent.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserWebService {
    @GET("api/user/all")
    List<User> getAllCategory();
    @GET("api/user/{id}")
    Call<User> getUserById(@Path("id") String id);
    @GET("api/user/login")
    Call<User> getUser();

    @POST("api/user")
    User addUser(@Body User event);

    @DELETE("api/user/{id}")
    void deleteUserById(@Path("id") String id);
}
