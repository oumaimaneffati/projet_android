package com.example.montagarfa.tunevent.ui.recyclerView;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.ui.activities.EventActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<HomeRecyclerViewAdapter.ViewHolder>{


    private static String TAG = "HomeRecyclerViewAdapter";
    private Context mContext;
    private ArrayList<Event> lEvent = new ArrayList<Event>();

    public HomeRecyclerViewAdapter(Context mContext, ArrayList<Event> lEvent) {
        this.mContext = mContext;
        this.lEvent = lEvent;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.event_cardview,parent,false);
       ViewHolder viewHolder =new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG,"On bind view holder : called");
        Glide.with(mContext)
                .asBitmap()
                .load(lEvent.get(position).getPhoto_uri())
                .into(holder.eventImage);
        holder.eventTitle.setText(lEvent.get(position).getNameEvent());
        DateFormat formatData = new SimpleDateFormat("dd-MMM-yy");
        String dateString = formatData.format(lEvent.get(position).getDate());
        Log.d(TAG,dateString);
        holder.eventDay.setText(dateString.substring(0,2));
        holder.eventMouth.setText(dateString.substring(3,6));
        holder.eventLocation.setText(lEvent.get(position).getLocation());
        holder.eventContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"On Click: clicked on: "+ lEvent.get(position).getNameEvent());

                Toast.makeText(mContext, lEvent.get(position).getNameEvent(),Toast.LENGTH_SHORT).show();
                Intent   intent= new Intent(mContext,EventActivity.class);
                intent.putExtra("event",lEvent.get(position));
                mContext.startActivity(intent);
            }

        });
    }

    @Override
    public int getItemCount() {
        return lEvent.size();
    }


    public class  ViewHolder extends RecyclerView.ViewHolder {
        ImageView eventImage;
        TextView eventTitle;
        TextView eventDay;
        TextView eventMouth;
        TextView eventLocation;
        LinearLayout eventContainer;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            eventContainer = itemView.findViewById(R.id.event_container1);
            eventImage = itemView.findViewById(R.id.event_image1);
            eventTitle= itemView.findViewById(R.id.event_title1);
            eventDay= itemView.findViewById(R.id.event_day1);
            eventMouth= itemView.findViewById(R.id.event_mouth);
            eventLocation= itemView.findViewById(R.id.event_location1);
        }
    }
}
